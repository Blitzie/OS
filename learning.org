# Local IspellDict: en
#+STARTUP: showeverything
#+SPDX-FileCopyrightText: 2018 Jens Lechtenbörger <https://lechten.gitlab.io/#me>
#+SPDX-License-Identifier: CC-BY-SA-4.0

#+INCLUDE: "config.org"

# Create agenda slide with 1st-level headings.
#+OPTIONS: toc:1

# Do not show section numbers.
#+OPTIONS: num:nil

#+TITLE: Learning and Teaching
#+AUTHOR: Jens Lechtenbörger
#+DATE: Computer Structures and Operating Systems 2018
#+REVEAL_ACADEMIC_TITLE: Dr.
#+KEYWORDS: learning, teaching, active learning, deliberate practice
#+DESCRIPTION: Explanation of active learning and teaching with flipped classroom and JiTT

* Introduction

** Introductory Activity
   #+ATTR_REVEAL: :frag (appear)
   1. Think of something you are really good at
      - Write it down (won’t be shared with anyone)
   2. Briefly describe how you got to be good at that thing
      - One or two words
   3. Submit how you got to be good at
      [[https://pingo.upb.de/727827][Pingo]] (pingo.upb.de → 727827)
      [[./img/qr-pingo-csos2018.png]]

   #+ATTR_HTML: :class slide-source
   (Source of activity: cite:SEI14)

** Brain ≈ Muscle
   #+ATTR_REVEAL: :frag (appear)
   - Learning involves brain’s *long term memory*
     {{{reveallicense("./figures/3d-man/brain_teacher.meta")}}}
   - Long term memory needs *repeated* retrieval and practice
     - Spaced out *over time*
     - Effect: Changes in brain’s *proteins*
   - (Learning does *not* happen [solely] in lectures)

** Deliberate Practice
   Characteristics of *Deliberate Practice* to acquire expert skills
   (cite:Eri08, see also cite:EKT93,SEI14)

   1. Task with *well-defined goal*
   2. Individual *motivated* to improve
   3. *Feedback* on current performance
   4. Ample opportunities for *repetition* and *gradual refinements*

   #+ATTR_REVEAL: :frag (appear)
   [[color:gray][(Traditional lecturing is “teaching by telling”, does not share *any* characteristic of Deliberate Practice)]]

** Active Learning
   - *Active Learning* increases student performance in science,
     engineering, and mathematics (cite:FEM+14)
     - Active Learning is an umbrella term for diverse
       interventions
       - Group problem-solving
       - Worksheets or tutorials completed during class
       - Use of personal response systems with or without peer instruction
       - Studio or workshop course designs
     - Notice: Above interventions share at least 3 of the 4
       characteristics of Deliberate Practice
       - (Motivation may increase, but ultimately rests with *you*)

** Quotes from Experts
   - On cite:FEM+14
     - Carl Wieman, Nobel Prize in Physics 2001
       #+ATTR_REVEAL: :frag (appear)
       - “[[https://blogs.scientificamerican.com/budding-scientist/stop-lecturing-me-in-college-science/][A lecture is basically a talking textbook]]”
       - In cite:Wie14: “However, in undergraduate STEM education, we have the curious situation that, although more effective teaching methods have been overwhelmingly demonstrated, most STEM courses are still taught by lectures—the pedagogical equivalent of bloodletting.”
     #+ATTR_REVEAL: :frag appear
     - Eric Mazur, Harvard physicist
       - “[[https://www.sciencemag.org/news/2014/05/lectures-arent-just-boring-theyre-ineffective-too-study-finds][This is a really important article—the impression I get is that it’s almost unethical to be lecturing if you have this data]]”
   #+ATTR_REVEAL: :frag appear
   - cite:SR17: “Saying Goodbye to Lectures
     in Medical School—Paradigm Shift or Passing Fad?”
     - “60 slides in 45 minutes may seem like an efficient way to
       teach, but it is unlikely to be an effective way to learn”

* Research on Motivation

** Self-Regulation
   :PROPERTIES:
   :CUSTOM_ID: self-regulation
   :END:
   #+ATTR_REVEAL: :frag (appear)
   - “Self-regulation is the process by which people change their
     beliefs and actions in the pursuit of their goals”
     #+ATTR_REVEAL: :frag appear
     - What are your *goals*?
   - Self-regulation “is known to support achievement in academic settings”
     #+ATTR_REVEAL: :frag appear
     - When and how do you *plan to engage* with course content?

   #+ATTR_HTML: :class slide-source
   (Source: cite:KC17)

** Goals
   - Individuals strive for different classes of achievement goals:
     mastery vs performance
     #+ATTR_REVEAL: :frag (appear)
     - *Mastery* (or learning) goals
       - Desire to develop *understanding*, improve *skills*
       - Belief that higher effort leads to better outcome
       - “I belong here”
     - *Performance* goals
       - Performance-approach goals
	 - Focus on positive outcomes, desire to perform *better than
           others*, paired with public recognition
	 - *Positive* effect on graded performance
       - Performance-avoidance goals
	 - Grounded in fear of failure, low competency expectancies
	 - *Negative* effect on graded performance

   #+ATTR_HTML: :class slide-source
   (Source: cite:Ame92,EC97)

** Mental Contrasting with Implementation Intentions (MCII)
   :PROPERTIES:
   :CUSTOM_ID: MCII
   :END:
   - Two complementary [[#self-regulation][self-regulation]] strategies
     1. *Mental Contrasting* (MC)
	- Goal commit and goal striving
	- Elaborate on positive outcomes of goals and potential obstacles
     2. *Implementation Intentions* (II)
	- Plan how to overcome obstacles
	- Create if-then plans, in particular for
	  1. getting started and
	  2. staying on track

   #+ATTR_HTML: :class slide-source
   (Source: cite:KC17)

*** MCII Intervention
    - MCII as *online survey* in Learnweb
      - Your participation serves as exercise in
	[[#self-regulation][self-regulation]].
      - Studies demonstrate more successful goal pursuit after MCII
	interventions in various settings (cite:KC17,DGL+11).
    - That survey is a *precondition* for access to material in Learnweb.
      - We review your responses to ensure “serious” answers.
	- Your answers do *not* have any influence on our grading!
      - We plan to repeat such surveys.

* CSOS Instruction

** CS: Flipped/Inverted Classroom with Projects
   - Videos instead of lectures
     - To be watched ahead of class meetings
     - Your pace based on your individual background and preferences
   - Projects to build computer from individual logical gates
     - Self-study
   - Lectures to discuss questions and work on exercises

   #+ATTR_HTML: :class slide-source
   (Scientific background: cite:LPT00,BV13)

** OS: Just-In-Time Teaching
   - Book and presentations with audio instead of lectures
     - To be worked through ahead of class meetings
   - Presentations contain exercises and assignments
     - To be submitted in Learnweb ahead of class meetings
     - Initiate feedback cycle out-of-class for more effective use of
       valuable in-class time

   #+ATTR_HTML: :class slide-source
   (Scientific background: cite:NPG+99,Nov18,MSN16)

#+INCLUDE: "backmatter.org"
