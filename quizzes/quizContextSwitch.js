quizContextSwitch = {
    "info": {
        "name":    "", // Should be empty with emacs-reveal
        "main":    "Who switches when?",
        "level1":  "Excellent!",          // 80-100%
        "level2":  "Please re-try.",      // 60-79%
        "level3":  "Please re-try.",      // 40-59%
        "level4":  "Maybe ask for help?", // 20-39%
        "level5":  "Please ask for help." //  0-19%, no comma here
    },
    "questions": [
	{
            "q": "Select correct statements about the infinite loops.",
            "a": [
                {"option": "voluntary_ctxt_switches increases dominantly for loop 1.", "correct": false},
                {"option": "voluntary_ctxt_switches increases dominantly for loop 2.", "correct": true},
                {"option": "nonvoluntary_ctxt_switches increases dominantly for loop 1.", "correct": true},
                {"option": "nonvoluntary_ctxt_switches increases dominantly for loop 2.", "correct": false},
                {"option": "Loop 1 is an example for a CPU bound execution.", "correct": true},
                {"option": "Loop 2 is an example for a CPU bound execution.", "correct": false}
            ],
            "correct": "<p><span>Correct!</span></p>",
            "incorrect": "<p><span>No. (Hint: 3 statements are correct.)</span> Do you know what sleeping is?</p>" // no comma here
        }
    ]
};
